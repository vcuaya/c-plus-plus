
vector<int> compareTriplets(vector<int> a, vector<int> b)
{
	// Vector <result> de tipo entero a retornar
	vector<int> result;

	// Inicialización con valor cero para los puntajes de cada competidor o retador
	result.push_back(0);
	result.push_back(0);

	// Se recorren los vectores recibidos <a> y <b>
	for (int i = 0; i < 3; i++)
	{
		// Si el puntaje de <a> es mayor que <b>
		if (a[i] > b[i])
		{
			// <a> recibe un punto
			result[0] += 1;
		}
		// Si el puntaje de <a> es menor que <b>
		if (a[i] < b[i])
		{
			// <b> recibe un punto
			result[1] += 1;
		}
	}

	// Se retorna el total de puntos
	return result;
}