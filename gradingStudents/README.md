# HackerRank #

## Problem Solving: Grading Students ##

Given the initial value of $ grade $ for each of Sam's $ n $ students, write code to automate the rounding process. 

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)