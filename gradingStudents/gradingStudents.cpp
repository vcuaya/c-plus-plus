vector<int> gradingStudents(vector<int> grades)
{
	// Variable a retornar
	vector<int> result;

	// Ciclo para recorre el arreglo <grades>
	for (int i = 0; i < grades.size(); i++)
	{
		// Variable para almacenar el residuo del elemento <grades[i]>
		int remainder = grades[i] % 5;

		// Variable para almacenar el próximo número múltiplo de 5
		int nextNumber = grades[i] + 5 - remainder;

		// Si el número <grades[i]> es mayor o igual que 38
		// y
		// el próximo número múltiplo de 5 menos el número <grades[i]> es menor que 3
		if (grades[i] >= 38 && (nextNumber - grades[i] < 3))
		{
			// Se agrega el múltiplo de 5 al vector
			result.push_back(nextNumber);
		}
		// Si no se cumple la condición
		else
		{
			// Se agrega el número <grades[i]> al vector
			result.push_back(grades[i]);
		}
	}

	// Se retorna el vector con los nuevos valores
	return result;
}