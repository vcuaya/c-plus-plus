# Programación Orientada a Objetos I #

Implementar en C++ el siguiente diagrama de clases:

\*************************************************
\*                    Dinero                     *
\*************************************************
\*                                               *
\* - pesos: int                                  *
\* - centavos: int                               *
\*                                               *
\*************************************************
\*                                              **
\* + Dinero()                                    *
\* + Mostrar(): void                             *
\* + IncrementarCentavos(): boolean              *
\* + IncrementarCentavos(centavos: int): boolean *
\* + IncrementarPesos(): boolean                 *
\* + IncrementarPesos(pesos: int): boolean       *
\*                                               *
\*************************************************

### Características del programa ###

El usuario puede:
* Incrementar su cuenta en una unidad, ya sean pesos o centavos
* Aumentar el saldo en pesos y centavos por la cantidad deseada usando valores enteros
* Mostar el saldo actual

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/c-plus-plus/)