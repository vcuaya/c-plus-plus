#include <iostream>
#include "Dinero.h"

void Cabecera()
{
	system("cls");
	std::cout << "Banco de Ejercicios";
	std::cout << "\nClase Dinero\n";
}

void Menu()
{
	Cabecera();
	std::cout << "\n1) Incrementar Centavos";
	std::cout << "\n2) Incementar Pesos";
	std::cout << "\n3) Ingresar Centavos";
	std::cout << "\n4) Ingresar Pesos";
	std::cout << "\n5) Mostrar Dinero";
	std::cout << "\n0) Salir" << std::endl;
	std::cout << "\nIntroduce la opcion deseada\n";
}

int main()
{
	Dinero cuenta;
	int pesos, centavos;
	char opc = 0;
	do
	{
		Menu();
		std::cin >> opc;
		switch (opc)
		{
		case '1':
			Cabecera();
			std::cout << "\nIncremento unitario en centavos\n";
			cuenta.Mostrar();
			std::cout << std::endl << std::endl;
			cuenta.IncrementarCentavos();
			std::cout << "Se realizo el incremento correctamente\n";
			cuenta.Mostrar();
			std::cout << std::endl << std::endl;
			system("pause");
			break;
		case '2':
			Cabecera();
			std::cout << "\nIncremento unitario en pesos\n";
			cuenta.Mostrar();
			std::cout << std::endl;
			cuenta.IncrementarPesos();
			std::cout << "\nSe realizo el incremento correctamente\n";
			cuenta.Mostrar();
			std::cout << std::endl << std::endl;
			system("pause");
			break;
		case '3':
			Cabecera();
			std::cout << "\nIngresa los centavos a incrementar\n";
			std::cin >> centavos;
			if (cuenta.IncrementarCentavos(centavos))
			{
				std::cout << "\nSe realizo el incremento correctamente\n";
				cuenta.Mostrar();
				std::cout << std::endl
					<< std::endl;
			}
			else
				std::cout << "\nNo se pudo realizar el incremento, intenta nuevamente\n\n";
			system("pause");
			break;
		case '4':
			Cabecera();
			std::cout << "\nIngresa los pesos a incrementar\n";
			std::cin >> pesos;
			if (cuenta.IncrementarPesos(pesos))
			{
				std::cout << "\nSe realizo el incremento correctamente\n";
				cuenta.Mostrar();
				std::cout << std::endl
					<< std::endl;
			}
			else
				std::cout << "\nNo se pudo realizar el incremento, intenta nuevamente\n\n";
			system("pause");
			break;
		case '5':
			Cabecera();
			std::cout << "\nEl total de la cuenta es:\n";
			cuenta.Mostrar();
			std::cout << std::endl
				<< std::endl;
			system("pause");
			break;
		case '0':
			system("cls");
			std::cout << "Programa finalizado!\n";
			break;
		default:
			system("cls");
			std::cout << "\a\a\aPor favor introduce una opcion valida\n";
			std::cout << std::endl;
			system("pause");
			break;
		}
	} while (opc != '0');
	return 0;
}