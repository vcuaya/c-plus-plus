#pragma once

class Dinero
{
	int pesos, centavos;

public:
	Dinero();
	void Mostrar();
	bool IncrementarCentavos();
	bool IncrementarCentavos(int);
	bool IncrementarPesos();
	bool IncrementarPesos(int);
};
