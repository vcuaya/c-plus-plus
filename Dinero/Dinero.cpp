#include "Dinero.h"
#include <iostream>

Dinero::Dinero()
{
	pesos = centavos = 0;
}

void Dinero::Mostrar()
{
	if (centavos < 10)
		std::cout << "$" << pesos << ".0" << centavos;
	else
		std::cout << "$" << pesos << "." << centavos;
}

bool Dinero::IncrementarCentavos()
{
	centavos++;
	if (centavos >= 100)
	{
		pesos++;
		centavos -= 100;
	}
	return true;
}

bool Dinero::IncrementarCentavos(int centavos)
{
	if (centavos > 0)
	{
		this->centavos = centavos;
		while (this->centavos >= 100)
		{
			pesos++;
			this->centavos -= 100;
		}
		return true;
	}
	else
		return false;
}

bool Dinero::IncrementarPesos()
{
	pesos++;
	return true;
}

bool Dinero::IncrementarPesos(int pesos)
{
	if (pesos > 0)
	{
		this->pesos += pesos;
		return true;
	}
	else
		return false;
}
