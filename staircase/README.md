# HackerRank #

## Problem Solving: Staircase ##

Write a program that prints a staircase of size $ n $.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)