void staircase(int n)
{
	// Ciclo for que va desde <0> hasta <n>
	for (int i = 0; i < n; i++)
	{
		// Variable a imprimir
		string result = "";

		// Índice auxiliar para identificar el número de espacios a agregar por línea
		int j = n - 1 - i;

		// Ciclo para agregar los espacios
		while (j > 0)
		{
			// Concatenación de espacios
			result += " ";

			// Decremento del índice auxiliar
			j--;
		}

		// Índice auxiliar para identificar el número de almohadillas a agregar por línea
		int k = i;

		// Ciclo para agregar las almohadillas
		while (k >= 0)
		{
			// Concatenación de almohadillas
			result += "#";

			// Decremento del índice auxiliar
			k--;
		}

		// Impresión de la cadena
		cout << result << endl;
	}
}