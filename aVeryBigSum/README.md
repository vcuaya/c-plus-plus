# HackerRank #

## Problem Solving: A Very Big Sum ##

Complete the aVeryBigSum function in the editor below. It must return the sum of all array elements.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)