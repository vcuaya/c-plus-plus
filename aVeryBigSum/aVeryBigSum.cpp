long aVeryBigSum(vector<long> ar)
{
	// Variable a retornar
	long result = 0;

	// Ciclo for para sumar cada elemento del arreglo
	// Se crea un iterador para recorrer el vector
	// La palabra reservada <auto> asigna el tipo de variable utilizada en la declaración
	// En este caso el iterador es de tipo <long>
	for (auto it = ar.begin(); it != ar.end(); it++)
	{
		// Se suma el contenido de la posición que apunta el iterador
		result += *it;
	}

	// Se regresa el resultado
	return result;
}