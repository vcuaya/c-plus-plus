int birthdayCakeCandles(vector<int> candles)
{
	// La palabra reservada <auto> asigna el tipo de variable utilizada en la declaración
	// La función <max_element> regresa la posición del elemento máximo o mayor
	auto maxIterator = max_element(candles.begin(), candles.end());

	// Identificada la posición del elemento máximo o mayor se asigna a otra variable
	int max = *maxIterator;

	// Se retorna el resultado de la función <count> la cual recorre el vector y cuenta los elementos iguales a <max>
	return count(candles.begin(), candles.end(), max);
}