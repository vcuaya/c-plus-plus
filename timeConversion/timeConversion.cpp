string timeConversion(string s)
{
	// Variable que almacena el substring <str_hours> que contiene solo la hora de la cadena principal <s>
	string str_hours = s.substr(0, 2);

	// Variable que almacena la conversión de tipo <string> a <int>
	int int_hours = stoi(str_hours);

	// Variable que almacena el valor booleano para determinar si la hora es PM o AM
	bool is_pm = (s[s.size() - 2] == 'P');

	// Si es PM y la hora es menor a doce
	if (is_pm && int_hours < 12)
	{
		// Se suma 12 para obtener el valor en formato de 24hrs
		int_hours += 12;
	}

	// Si no es PM y la hora es igual a doce
	if (!is_pm && int_hours == 12)
	{
		// Se asigna 0 a la hora para obtener el valor en formato de 24hrs
		int_hours = 0;
	}

	// Se convierte la nueva hora en string
	str_hours = to_string(int_hours);

	// Si la hora es menor 10
	if (str_hours.length() < 2)
	{
		// Se agrega un cero al inicio de la cadena
		str_hours = "0" + str_hours;
	}

	// Se concatena la hora convertida y la subcadena de la cadena original
	// omitiendo las primeras dos posiciones que pertenecen a la hora
	// y las dos últimas posiciones que pertenecen al formato AMP/PM
	str_hours += s.substr(2, 6);

	// Se retorna la nueva cadena
	return str_hours;
}