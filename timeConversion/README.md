# HackerRank #

## Problem Solving: Time Conversion ##

Given a time in -hour AM/PM format, convert it to military (24-hour) time.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)