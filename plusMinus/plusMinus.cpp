void plusMinus(vector<int> arr)
{
	// Impresión en pantalla del total de elementos positivos
	cout << static_cast<double>(count_if(arr.begin(), arr.end(), [](int x){ return 0 < x; })) / arr.size() << endl;
	
	// Impresión en pantalla del total de elementos negativos
	cout << static_cast<double>(count_if(arr.begin(), arr.end(), [](int x){ return x < 0; })) /arr.size()<< endl;
	
	// Impresión en pantalla del total de elementos iguales a cero
	cout << static_cast<double>(count_if(arr.begin(), arr.end(), [](int x){ return x == 0; })) / arr.size();

	// Para cada impresión se utiliza la función <count_if(it_inicio, it_fin, predicado)> donde el parámetro predicado
	// es una función anónima o lambda con la siguiente sintaxis <[](){}> donde
	// [] es el contenedor que almacenara lo que retorne la función
	// () es el parametros o parametros a utilizar por la función 
	// {} es el cuerpo de la función
	// Obtenido el total de elementos de cada caso se obtiene la proporción que representan del arreglo
	// Y finalmente se imprimen en pantalla, se debe realizar un casteo para que se impriman los valores racionales
	// de lo contrario solo se visualizará el valor entero de la proporcion, es decir, cero.
}