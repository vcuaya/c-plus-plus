# HackerRank #

## Problem Solving: Plus Minus ##

Given an array of integers, calculate the ratios of its elements that are positive, negative, and zero. Print the decimal value of each fraction on a new line with places after the decimal.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)