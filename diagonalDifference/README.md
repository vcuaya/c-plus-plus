# HackerRank #

## Problem Solving: Diagonal Difference ##

Given a square matrix, calculate the absolute difference between the sums of its diagonals.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)