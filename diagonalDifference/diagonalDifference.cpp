int diagonalDifference(vector<vector<int>> arr)
{
	// Variable para almacenar la suma de la diagonal principal
	int left_to_right = 0;

	// Variable para almacenar la suma de la diagonal secundaria
	int right_to_left = 0;

	// Ciclo para recorrer ambas diagonales
	for (int i = 0; i < arr.size(); i++)
	{
		// Se suman los elementos de la diagonal principal
		left_to_right += arr[i][i];

		// Se suman los elementos de la diagonal secundara
		// El método <.size()> retorna el número de vectores dentro del vector
		// Dado que es una matriz cuadrada el número de filas también es el número de columnas
		// Se resta una unidad ya que el índice <i> de un arreglo inicia en cero
		// Se resta <i> para asegurar que solo se tomen los elementos de la diagonal secundaria
		right_to_left += arr[i][arr.size() - i - 1];
	}

	// Se regresa el valor absoluto de la resta de ambas diagonales
	return abs(left_to_right - right_to_left);
}