# C++ Programs #

This repository contains some codes on structure programming and object-oriented programming using the C++ language. They were originally programed using Codeblocks, but for didactic reasons I will re-program them using Visual Studio Community.

### I hope this repository will be useful to you. ###

## Authors

### V�ctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)