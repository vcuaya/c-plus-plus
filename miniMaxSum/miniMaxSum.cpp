void miniMaxSum(vector<int> arr)
{
	// Variable para almacenar la suma de los valores menores del vector
	long min = 0;

	// Variable para almacenar la suma de los valores mayores del vector
	long max = 0;

	// Se ordena el vector para que cada ciclo <for> sume correctamente los elementos
	sort(arr.begin(), arr.end());

	// Ciclo para los elementos menores
	for (int i = 0; i < arr.size() - 1; i++)
	{
		// Suma de las posiciones [0] a [n-2] donde <n> es el tamaño del vector
		min += arr[i];
	}

	// Ciclo para los elementos mayores
	for (int i = arr.size() - 1; i > 0; i--)
	{
		// Suma de las posiciones [1] a [n-1] donde <n> es el tamaño del vector
		max += arr[i];
	}

	// Impresión en pantalla de la suma de los valores mínimo y máximo
	cout << min << " " << max;
}