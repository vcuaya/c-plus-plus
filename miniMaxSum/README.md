# HackerRank #

## Problem Solving: Mini-Max Sum ##

Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)