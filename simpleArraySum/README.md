# HackerRank #

## Problem Solving: Simple Array Sum ##

Given an array of integers, find the sum of its elements.

### I hope this repository will be useful to you. ###

## Authors

### Víctor Cuaya

[Contact me](mailto:vcuaya@outlook.com)

[Bitbucket Repository](https://bitbucket.org/vcuaya/)