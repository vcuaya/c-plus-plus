int simpleArraySum(vector<int> ar)
{
	// Variable a retornar
	int res = 0;

	// Ciclo para recorrer el arreglo <ar>
	// La palabra reservada <auto> asigna el tipo de variable utilizada en la declaración
	// En este caso el iterador es de tipo <int>
	for (auto it = ar.begin(); it != ar.end(); it++)
	{
		// Suma del contenido del iterador para cada elemento del vector
		res += *it;
	}

	// Retorno de la respuesta
	return res;
}